<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>My Test</title>

<link rel="icon" href="{{ asset('assets/images/favicon.ico') }}">
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/fullcalendar/fullcalendar.print.css') }}" rel='stylesheet' media='print'>
<link href="https://www.jqueryscript.net/demo/Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="{{ asset('assets/css/extra-theme.css') }}" rel="stylesheet">
<link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/jb-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
</head>
<body>
<div id="wrapper">
<nav class="navbar-default navbar-static-side" role="navigation">
  <div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">
      <li class="nav-header">
        <div class="dropdown profile-element"> <img alt="image" class="rounded-circle pr-pic-sm" src="{{asset('assets/images/avatars/no-image-icon.png')}}" /> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span class="block m-t-xs font-bold">Admin<b class="caret"></b></span> <span class="text-muted text-xs block">admin@gmail.com</span> </a>
          <ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><a class="dropdown-item" href="{{ route('profile_detail') }}">Profile</a></li>
            <li><a class="dropdown-item" href="#">Change Password</a></li>
            <li class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-power-off"></i>  {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form></li>
          </ul>
        </div>
        <div class="logo-element"> MyTEST+ </div>
      </li>

        <li> <a href="/user_management"><i class="fa fa-cog" aria-hidden="true"></i> <span class="nav-label">User Management</span></a> </li>
     
    </ul>
  </div>
  <div class="lft-jbg animated fadeInLeft"></div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
  <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> </div>
    <div class="cl-wrp">
      <div class="cl-wrp-p0"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
      <div class="cl-wrp-pl"></div>
      <iframe scrolling="no" frameborder="no" clocktype="html5" style="overflow:hidden;border:0;margin:0;padding:0;width:240px;height:25px;"src="https://www.clocklink.com/html5embed.php?clock=018&timezone=UnitedArabEmirates_AbuDhabi&color=gray&size=240&TimeFormat=hh:mm:ss TT&Color=gray"></iframe>
    </div>
    <ul class="nav navbar-top-links navbar-right">
      <li> <span class="m-r-sm text-muted welcome-message">Welcome</span> </li>
      <li><a href="#"><i class="fa fa-bell"></i>&nbsp;Help</a> </li>
      <li><a  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form-1').submit();">
                                        <i class="fa fa-power-off"></i>  {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form-1" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
      
       <!-- <a href="/login"> <i class="fa fa-power-off"></i> Log out </a>  -->
       </li>
    </ul>
  </nav>
</div>

@yield('content')


<div class="footer">
 <div> &copy;2020 All Rights Reserved. Powered by: &nbsp;<a target="_blank" href="hi">hi</a> </div>
 </div>
 </div>
</div>
<!-- Mainly scripts -->
<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/peity/jquery.peity.min.js') }}"></script>

<!-- Data picker -->
<!-- Custom and plugin javascript -->
<script src="{{ asset('assets/js/inspinia.js') }}"></script>
<script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/typehead/bootstrap3-typeahead.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
<!-- jQuery UI  -->
<script src="{{ asset('assets/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/touchpunch/jquery.ui.touch-punch.min.js') }}"></script>
<script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>
<script src="{{ asset('assets/js/plugins/assets/scripts-init/demo.js') }}"></script>
<!--Apex Charts-->
<script src="{{ asset('assets/js/plugins/assets//vendors/charts/apex-charts.js') }}"></script>
<script src="{{ asset('assets/js/plugins/assets/scripts-init/charts/apex-charts.js') }}"></script>
<script src="{{ asset('assets/js/plugins/assets/scripts-init/charts/apex-series.js') }}"></script>
<!--Sparklines-->
<script src="{{ asset('assets/js/plugins/assets/vendors/charts/charts-sparklines.js') }}"></script>
<script src="{{ asset('assets/js/plugins/assets/scripts-init/charts/charts-sparklines.js') }}"></script>
<!--Chart.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="{{ asset('assets/js/plugins/assets/scripts-init/charts/chartsjs-utils.js') }}"></script>
<script src="{{ asset('assets/js/plugins/assets/scripts-init/charts/chartjs.js') }}"></script>
<!--Multiselect-->
<script src="{{ asset('assets/js/plugins/assets/vendors/form-components/bootstrap-multiselect.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset('assets/js/plugins/assets/scripts-init/form-components/input-select.js') }}"></script>
<!--Toggle Switch -->
<script src="{{ asset('assets/js/plugins/assets/vendors/form-components/toggle-switch.js') }}"></script>
<!--Circle Progress -->
<script src="{{ asset('assets/js/plugins/assets/vendors/circle-progress.js') }}"></script>
<script src="{{ asset('assets/js/plugins/assets/scripts-init/circle-progress.js') }}"></script>
<script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/js/moment.js') }}"></script>
<script src="https://www.jqueryscript.net/demo/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('assets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-tooltip.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-confirmation.js') }}"></script>
<script src="{{ asset('assets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        //hides dropdown content
        $(".size_chart").hide();

        //unhides first option content
        $("#option1").show();

        //listen to dropdown for change
        $("#size_select").change(function() {
            //rehide content on change
            $('.size_chart').hide();
            //unhides current item
            $('#' + $(this).val()).show();
        });

        $('#data_1 .input-group.date').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'ExampleFile'
                },
                {
                    extend: 'pdf',
                    title: 'ExampleFile'
                },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });

        $("#fold").click(function() {
            $("#fold_p").fadeOut(function() {
                $("#fold_p").text(($("#fold_p").text() == 'Hide Filter') ? 'Show Filter' : 'Hide Filter').fadeIn();
            })
        })

        $("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 48], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#1ab394',
            fillColor: "transparent"
        });

        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker();
        $('input[name="daterange"]').daterangepicker();
    });
</script>

@yield('script')

</body>
</html>
