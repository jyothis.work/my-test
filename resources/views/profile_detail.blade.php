@extends('layouts.inner')

@section('content')

<link href="{{ asset('assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-3">
    <h2>Profile Detail</h2>
  </div>
  <div class="col-lg-9 mnb-wrp"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row m-b-lg m-t-lg">
    <div class="col-md-6">
      <div class="profile-image"> <img src="{{ (Auth::user()->image_name == null) ? asset('assets/images/avatars/no-image-icon.png') : asset('/storage/upload/'.Auth::user()->image_name)  }}" class="rounded-circle circle-border m-b-md" alt="profile"> </div>
      <div class="profile-info">
        <div class="">
          <div>
            <h2 class="no-margins"> {{$user->first_name." ".$user->last_name }} </h2>
            <!-- <small> There are many variations of passages of Lorem Ipsum available, but the majority
            have suffered alteration in some form Ipsum available. </small>--> </div> 
        </div>
      </div>
    </div>
    <!-- <div class="col-md-3">
      <table class="table small m-b-xs">
        <tbody>
          <tr>
            <td><strong>142</strong> Projects </td>
            <td><strong>22</strong> Followers </td>
          </tr>
          <tr>
            <td><strong>61</strong> Comments </td>
            <td><strong>54</strong> Articles </td>
          </tr>
          <tr>
            <td><strong>154</strong> Tags </td>
            <td><strong>32</strong> Friends </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-3"> <small>Sales in last 24h</small>
      <h2 class="no-margins">206 480</h2>
      <div id="sparkline1"></div>
    </div> -->
  </div>
  <div class="row">
    <div class="col-lg-12 prsec">
      <div class="tabs-container">
        <ul class="nav nav-tabs" role="tablist"  style="margin-bottom: 0rem !important;">
          <li><a class="nav-link active" data-toggle="tab" href="#tab-1"> General</a></li>
          <!-- <li><a class="nav-link" data-toggle="tab" href="#tab-2">Advanced</a></li> -->
        </ul>
        <div class="tab-content">
          <div role="tabpanel" id="tab-1" class="tab-pane active">
            <div class="panel-body">
            <div class="row" style="padding-top:15px;">
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">First Name:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" value="{{$user->first_name}}" id="fname" name="fname" disabled>
                      @error('fname')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Last Name:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" value="{{$user->last_name}}" id="lname" name="lname" disabled>
                      @error('lname')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                  <label class="col-sm-3 col-form-label">Employee Id:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" value="{{$user->employee_id}}" id="empId" name="empId" disabled>
                      @error('empId')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Designation:</label>
                    <div class="col-sm-9">
                      <select class="form-control select-with-search" name="designation" id="designation" style="width:100% !important;" disabled>
                      <option value="0">--Please select designation--</option>
                        @if($designations)
                            @foreach($designations as $designation)

                              @if($user->designation_id == $designation->id) 
                                @php($selected = "selected") 
                              @else 
                                @php($selected = "")
                              @endif 


                             <option {{ $selected }} value="{{$designation->id}}">{{$designation->name}}</option>

                            @endforeach
                        @endif
                      </select>
                      @error('designation')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Deparment</label>
                    <div class="col-sm-9">
                      <select  class="form-control select-with-search" name="department" id="department" style="width:100% !important;" disabled>
                        <option value="0">--Please select department--</option>
                        @if($departments)
                            @foreach($departments as $department)

                            @if($user->department_id == $department->id) 
                                @php($selected = "selected") 
                              @else 
                                @php($selected = "")
                              @endif 

                             <option {{ $selected }} value="{{$department->id}}">{{$department->name}}</option>

                            @endforeach
                        @endif
                      </select>
                      @error('department')
                          <div class="alert-danger">{{ $message }}</div>
                       @enderror
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Email Address:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="email" value="{{$user->email}}" name="email" disabled>
                      @error('email')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
              
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Mobile Number:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="mobile" value="{{$user->mobile}}" name="mobile" disabled>
                      @error('mobile')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row" id="data_1">
                    <label class="col-sm-3 col-form-label">Hire Date:</label>
                    <div class="col-sm-9 input-group date"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      <input type="text" class="form-control" name="hireDate" id="hireDate" value="{{$user->hire_date}}" disabled>
                      @error('hireDate')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  
                  </div>
                 
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Pay Rate:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="payRate" name="payRate" value="{{$user->pay_rate}}" disabled>
                      @error('payRate')
                          <p class=" alert-danger">{{ $message }}</p>
                      @enderror
                    </div>
                   
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Eligible for overtime:</label>
                    <div class="col-sm-6">
                      <ul class="eo-list">
                        <li>
                          <label>
                          <input type="radio" {{ $user->overtime_status=="YES" ? "checked" : "" }}   name="overtimeStatus" value="yes">
                          Yes </label>
                        </li>
                        <li>
                          <label>
                          <input type="radio" {{ $user->overtime_status=="NO" ? "checked" : ""    }} name="overtimeStatus" value="no">
                          No </label>
                        </li>
                      </ul>
                    </div>
                     @error('overtimeStatus')
                          <p class="alert-danger">{{ $message }}</p>
                      @enderror
                  </div>
                </div>
                <!-- <div class="col-lg-12" style="text-align: right;">
                  <button type="submit" class="btn btn-success">Save Changes</button>
                </div> -->
              </div>
            </div>
          </div>
          <!-- <div role="tabpanel" id="tab-2" class="tab-pane">
            <div class="panel-body">
              <div class="tabs-container">
                <div class="tabs-left">
                  <ul class="nav nav-tabs">
                    <li><a class="nav-link active" data-toggle="tab" href="#tab-3">Permission</a></li>
                    <li><a class="nav-link" data-toggle="tab" href="#tab-4">Location Tracking</a></li>
                    <li><a class="nav-link" data-toggle="tab" href="#tab-5">Notification</a></li>
                  </ul>
                  <div class="tab-content ">
                    <div id="tab-3" class="tab-pane active">
                      <div class="panel-body">
                        <div class="col-md-5">
                          <fieldset>
                          <h4> Permission </h4>
                          <div class="ad_pr">
                            <ul>
                              <li>
                                <label> Role </label>
                              </li>
                              <li>
                                <select class="form-control">
                                  <option value="0">Please select</option>
                                  <option value="1">Admin</option>
                                  <option value="2">Manager</option>
                                </select>
                              </li>
                            </ul>
                          </div>
                          <div class="clearfix"></div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Company Management </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Mobile Time Entry </label>
                          </div>
                          <div class="clearfix"></div>
                          <div class="ad_pr">
                            <ul>
                              <li>
                                <label> Timesheet view </label>
                              </li>
                              <li>
                                <select class="form-control">
                                  <option value="0">Please select</option>
                                  <option value="1">Own Team</option>
                                  <option value="2">While Team</option>
                                  <option value="3">Department Wise</option>
                                </select>
                              </li>
                            </ul>
                          </div>
                          <div class="clearfix"></div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Amendmend to Timesheet </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox"  checked="">
                            <label for="checkbox2"> Team Management Authorisation </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Customer View </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox"  checked="">
                            <label for="checkbox2"> Customer Managers </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Schedule View </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Schedule Management </label>
                          </div>
                          <div class="bt-wrp">
                            <button class="btn btn-primary">Apply</button>
                          </div>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                    <div id="tab-4" class="tab-pane">
                      <div class="panel-body">
                        <div class="col-md-5">
                          <fieldset>
                          <h4> Location Tracking </h4>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Required </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox" checked="">
                            <label for="checkbox2"> Optional </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Off </label>
                          </div>
                          <div class="bt-wrp">
                            <button class="btn btn-primary">Apply</button>
                          </div>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                    <div id="tab-5" class="tab-pane">
                      <div class="panel-body">
                        <div class="col-md-5">
                          <fieldset>
                          <h4> Clock In/ Out </h4>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox" checked="">
                            <label for="checkbox2"> Company Value </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Custom value for per employee </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Mobile </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> Email </label>
                          </div>
                          <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2"> SMS </label>
                          </div>
                          <div class="bt-wrp">
                            <button class="btn btn-primary">Apply</button>
                          </div>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</div>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


@endsection