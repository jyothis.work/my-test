@extends('layouts.inner')

@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset('assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-3">
    <h2>User Management</h2>
  </div>
  <div class="col-lg-9 mnb-wrp"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
        <div class="col-lg-12">
            @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!</strong> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Failed!</strong> {{session('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif  
        </div>
  </div>
  <div class="row" style="display:none" id="user_add_div">
    <div class="col-lg-12 prsec">
      <div class="tabs-container">
        <ul class="nav nav-tabs" role="tablist"  style="margin-bottom: 0rem !important;">
          <li><a class="nav-link active" data-toggle="tab" href="#tab-1"> Edit</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" id="tab-1" class="tab-pane active">
          <form action="/user_management/save" method="post" id="userAdd" enctype='multipart/form-data'>
            @csrf
           <input type="hidden" name="edit_id" id="edit_id"  value="">
            <div class="panel-body">
              <div class="row">
              <p class="btn btn-lg btn-success" style="width:160px;height:40px">
                      Upload Profile image<input  style=" position: absolute;font-size: 50px;opacity: 0;right: 0;top: 0;" type="file" name="image_name" id="image_name" onchange="readURL(this);">
                    </p>
                    <img id="blah" src="" alt=""  class="rounded-circle">

              </div>
              <div class="row" style="padding-top:15px;">
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">First Name:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control"   id="fname" name="fname">
                      @error('fname')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Last Name:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="lname" name="lname">
                      @error('lname')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  
                  </div>
                </div>

                
               
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Email Address:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="email" name="email">
                      @error('email')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
              
                </div>
                <div class="col-lg-6">
                  <div class="form-group  row">
                    <label class="col-sm-3 col-form-label">Mobile Number:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="mobile" name="mobile">
                      @error('mobile')
                          <div class="alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  
                  </div>
                </div>
                
                
                
                
                
                <div class="col-lg-12" style="text-align: right;">
                  <button type="submit" class="btn btn-success">Save Changes</button>
                </div>
              </div>
            </div>
          </form>
        </div>
          
          
        </div>
      </div>
    </div>
  </div><br>
  <div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title" style="min-height: 55px;">
                <h5 style=" float: left; margin-right: 7px;">Users</h5>
                <button type="button" id="user_add_div_activate" class="btn btn-w-s btn-danger"><i class="fa fa-plus" aria-hidden="true"></i></button>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="dataTables-user" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                  <th>#</th>
                                  <th>FirstName</th>
                                  <th>LastName</th>
                                  <th>Email</th>
                                  <th>Mobile</th>
                                  <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($users)
                              @foreach($users as $key=>$user)
                                <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td>{{ $user->first_name }}</td>
                                  <td>{{ $user->last_name }}</td>
                                  <td>{{ $user->email }}</td>
                                  <td>{{ $user->mobile }}</td>
                                  <td>
                                  <a title="Edit this User" data-user_id="{{$user->id}}"   class="btn btn-round btn-info btn-xs user_edit" style="margin: 3px;">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                  </td>
                                </tr>
                              @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
//document.getElementById("defaultOpen").click();
</script>


@endsection

@section('script')
<script>
$(document).ready(function() {
    $('.select-with-search').select2();
});

$('#user_add_div_activate').on('click', function(){

  if ($("#user_add_div").is(":visible") == true){

      $(this).html('<i class="fa fa-plus" aria-hidden="true"></i>');
      $('#user_add_div').hide();

  } else {

      $(this).html('<i class="fa fa-minus" aria-hidden="true"></i>');
      $('#user_add_div').show();

  }
});

$('#dataTables-user').DataTable({
  pageLength: 10,
  responsive: true,
  dom: '<"html5buttons"B>lTfgitp',
  buttons: [{
          extend: 'copy'
      },
      {
          extend: 'csv'
      },
      {
          extend: 'excel',
          title: 'department'
      },
      {
          extend: 'pdf',
          title: 'department'
      },

      {
          extend: 'print',
          customize: function(win) {
              $(win.document.body).addClass('white-bg');
              $(win.document.body).css('font-size', '10px');

              $(win.document.body).find('table')
                  .addClass('compact')
                  .css('font-size', 'inherit');
          }
      }
  ]

});

$('.user_edit').on('click',function(){

  var id    = $(this).data("user_id");
  $('#page_permission_tab_link').show();

  $.ajax({
    url: "/edit_show/"+id,
    type: "get",
    success: function(data){

      $('#edit_id').val(id);

      $('#fname').val(data.first_name);
      $('#lname').val(data.last_name);



      $('#email').val(data.email);
      $('#mobile').val(data.mobile);



     

      $('#blah').attr('src', "{{asset('/storage/upload/')}}/"+ data.image_name);
      $('#blah').attr('width', 150);
      $('#blah').attr('height', 150);
      $.each(data.permission_ids, function(key,value) {
        $('#id_'+value).prop('checked', true);
      }); 
      $('#user_add_div').show();
      
    }

  });

});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
        $('#blah').attr('width', 150);
        $('#blah').attr('height', 150);

    };

    reader.readAsDataURL(input.files[0]);
  }
}

$("#page_permit_button").on('click',function(){

  var user_id       = $('#edit_id').val();

  var myCheckboxes  = new Array();
  $("input[name='page[]']:checked").each(function() {
    myCheckboxes.push($(this).val());
  });

  var myUnCheckboxes = new Array();
  $("input[name='page[]']:not(:checked)").each(function() {
    myUnCheckboxes.push($(this).val());
  })

  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({

    url  : "apply_permission",
    type : "post",
    data : { checked:myCheckboxes, unchecked:myUnCheckboxes, user_id:user_id },

    success:function(response){
      $.notify({
        // options
        message: response.message 
      },{
        // settings
        type: 'success'
      });
    }

  });

});
    
</script>
@endsection
