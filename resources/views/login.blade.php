@extends('layouts.outer')

@section('content')
<div class="cr-log middle-box text-center loginscreen animated fadeInDown">
  <div>
    <p>Welcome</p>
    <h3>Login</h3>
    <form class="m-t" method="post" role="form" action="{{ route('login') }}">
    @csrf
      <div class="form-group">
        <input id="email" type="text" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="" required autocomplete="email" autofocus>
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
      <div class="form-group">
        <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
      <!--<button type="submit" class="btn btn78 btn-primary block full-width m-b">Login</button>-->
      <!-- <a href="/home" class="btn btn78 btn-primary block full-width m-b">Login</a> <a href="#"><small>Forgot password?</small></a> -->
      <button type="submit" class="btn btn78 btn-primary block full-width m-b">
        {{ __('Login') }}
      </button> 
      <a href="#"><small>Forgot password?</small></a>
    </form>
    <p class="m-t"> <small> ©2019 All Rights Reserved. <a target="_blank" href="#">Hi Technologies</a> </small> </p>
  </div>
</div>
@endsection

