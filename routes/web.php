<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('login/user', function () {
    return view('login');
});

Route::get('login/admin', function () {
    return view('admin_login');
});

Route::post('/login/adminLogin',  [App\Http\Controllers\Auth\LoginController::class, 'adminLogin'])->name('admin');
Route::post('/login/user',  [App\Http\Controllers\Auth\LoginController::class, 'authenticate'])->name('login');
Route::post('/logout',  [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');


Route::get('home/admin',  [App\Http\Controllers\HomeController::class, 'indexAdmin'])->name('admin_home');
Route::get('home/user',  [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/user_management',  [App\Http\Controllers\UserController::class, 'index'])->name('user_management');
Route::post('/user_management/save',  [App\Http\Controllers\UserController::class, 'save']);
Route::get('/edit_show/{id}',  [App\Http\Controllers\UserController::class, 'editShow']);
Route::post('/apply_permission',  [App\Http\Controllers\UserController::class, 'savePermission']);

Route::get('/profile_detail',  [App\Http\Controllers\ProfileController::class, 'index'])->name('profile_detail');
