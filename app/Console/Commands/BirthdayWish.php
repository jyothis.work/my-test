<?php

namespace App\Console\Commands;
use App\Models\User;

use Illuminate\Console\Command;

class BirthdayWish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wish:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Birthday Wish for the User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $wish = "Happay Birthday";
         
        $users = User::all();
        foreach ($users as $user) {

            if($user->dob == date('Y-m-d')){
                $user->greeting = $wish;
                $user->greeting_status =  1;
                $user->save();
            }
            
        }
         
        $this->info('Successfully sent daily quote to everyone.');
    }
}
