<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;



class HomeController extends Controller
{

    public function index(){

        $users = User::all()->except(Auth::User()->id);

        $data= array("users"=> $users);
        
        return view('home')->with($data);
    }

    public function indexAdmin(){

        $users = User::all();

        $data= array("users"=> $users);
        
        return view('admin_home')->with($data);
    }

    
}
