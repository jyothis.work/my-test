<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class ProfileController extends Controller
{

    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){

        $data         = array('user'=>Auth::user());
        return view('profile_detail')->with($data);
    }
}
