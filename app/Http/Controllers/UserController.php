<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index(){

        $users        = User::all();

        $data         = array('users'=>$users);

        return view('user_management')->with($data);
    }

    public function save(Request $request){

        $data = $request->validate([
            'fname'          => 'required',
            'lname'          => 'required',
            'email'          => 'required',
            'mobile'         => 'required',
        ]);


        if ($request->filled('edit_id')) {

            $user_edit = User::find($request->input('edit_id'));

            $user_edit->first_name        = $request->input('fname');
            $user_edit->last_name         = $request->input('lname');
            $user_edit->email             = $request->input('email');
            $user_edit->mobile            = $request->input('mobile');
            $user_edit->password          = Hash::make('123');
            $user_edit->status            = 1;
            $user_edit->save();

            if ($request->hasFile("image_name")) {
                $extension = $request->file('image_name')->extension();
                $img_name = 'img_'.$user_edit->id.'.'.$extension;
                $path = $request->file('image_name')->storeAs('public/upload',$img_name);
                $user_edit->image_name = $img_name;
                $user_edit->save();
            }

            $request->session()->flash('success','User has been Edited successfully');
        
            return redirect('/user_management');

        } else {
            
            $user_new = new User;

            $user_new->first_name        = $request->input('fname');
            $user_new->last_name         = $request->input('lname');
            $user_new->email             = $request->input('email');
            $user_new->mobile            = $request->input('mobile');
            $user_new->password          = Hash::make('123');
            $user_new->status            = 1;
            $user_new->save();

            if ($request->hasFile("image_name")) {
                $extension = $request->file('image_name')->extension();
                $img_name = 'img_'.$user_new->id.'.'.$extension;
                $path = $request->file('image_name')->storeAs('public/upload',$img_name);
                $user_new->image_name = $img_name;
                $user_new->save();
            }

            $request->session()->flash('success','User has been Added successfully');
        
            return redirect('/user_management');

        }
    }

    public function editShow($id){

        $user = User::find($id);


        $temp = array();
        $temp['first_name'] = $user->first_name;
        $temp['last_name'] = $user->last_name;
        $temp['mobile'] = $user->mobile;
        $temp['email'] = $user->email;
        $temp['image_name'] = $user->image_name;
        return response()->json($temp);

    }

   
}
